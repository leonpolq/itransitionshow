var deepAssign = require('deep-assign')

export default class Filter{
	static get RESULT_NO(){
		return "THERE_ARE_NO_RESULTS"
	}; 
	initAttributes = [];
	filteredItems = [];
	storage = [];
	component;
	constructor(component){
		this.component = component;
		// console.log("!sdf");
	}

    setFilteredItems(initItems){
        this.initAttributes =  initItems;
        this.filteredItems = initItems;

        return this;
    }
 	_searchElements(data, attributes){
 		var elem = Object.assign({}, data);
 		var searchKey;
        var attributeNamesCopy = attributes.slice();
		while((searchKey = attributeNamesCopy.shift())!==undefined && typeof elem[searchKey] !== 'undefined' ){
			elem = elem[searchKey];
			// console.log(elem);
		} 
		if(!attributeNamesCopy.length){
			return elem;
		}else{
			return false;//this.RESULT_NO; 
		}
 	}
 	_getResultCopyObject(attributes, data){
		var array = attributes;
		var object = {};
		array.reduce(function(o, s) { 
			if(s == array[array.length-1])
				return o[s] = data; 
			else
				return o[s] = {}; 

		}, object);
		return object;

 	}
 	_removeValue(attributes, data){
        var attributeNamesCopy = attributes.slice();
        var _cloneObject =  function(obj){
		    if (obj === null || typeof obj !== 'object') {
		        return obj;
		    }
		    var temp = obj.constructor(); // give temp the original obj's constructor
		    for (var key in obj) {
		    	if(key == Object.values(attributeNamesCopy)[0]){
		    		attributeNamesCopy.shift();
		    		if(!attributeNamesCopy.length)
		        		temp[key] = [];
	    			else	
		        		temp[key] = _cloneObject(obj[key]);
		    	}else
		        	temp[key] = _cloneObject(obj[key]);
		    }
		 
		    return temp;
		};
	  	var newObj = _cloneObject(data);
	  	return newObj;

 	}
 	sortResults(func){
 		this.filteredItems.sort(func);
 		return this;
 	}
    searchInBranch(searchConfiguration, initValue=false, isLike = false, condition = false){
    	// {branch: ['invoices'], element:['chat_id']}
    	var newStorageElement = {
    		key: Object.assign({}, searchConfiguration),
    		value: initValue, 
    		items: []
    	};
    	var _this = this; 
    	var tmpItems = [];
    	for (var i = 0; i < this.filteredItems.length; i++) {
    		var elem;
			if((elem = this._searchElements(this.filteredItems[i], searchConfiguration.branch))!==false){
				newStorageElement.items.push(elem);
				// [{.. }, {.. }]
				elem = elem.filter((dataInner)=>{
					var elemInner;
					if((elemInner = this._searchElements(dataInner, searchConfiguration.element))!==false){
						if(condition){
							if(!_this.getValueByKey(searchConfiguration)){
								return true;
							}else{
								return condition(elemInner, _this.getValueByKey(searchConfiguration));
							}
						} else if(isLike){
							if(!_this.getValueByKey(searchConfiguration)){
								return true;
							}else{
								return elemInner.toString().toLowerCase().indexOf(_this.getValueByKey(searchConfiguration).toLowerCase())!==-1;
							}
						}else if(initValue){
							return elemInner === initValue;
						}else {
							if(!_this.getValueByKey(searchConfiguration)){
								return true;
							}else{
								return elemInner == _this.getValueByKey(searchConfiguration);
							}
						}
					}
				});

				// console.log(elem);
				if(elem.length){
					tmpItems.push(deepAssign({}, this._removeValue(searchConfiguration.branch,this.filteredItems[i]), this._getResultCopyObject(searchConfiguration.branch, elem)));
					//return data;		
				}
			}
		
    	}
    	this.filteredItems = tmpItems;
        this.storage.push(newStorageElement);
		return this; 	

    }
    filterAttribute(attributeNames, initValue = false, isLike=false){
    	var newStorageElement = {
    		key: attributeNames.slice(),
    		value: initValue,
    		items: []
    	};
    	var _this = this; 
        this.filteredItems = this.filteredItems.filter((data)=>{
            var elem = Object.assign({}, data), searchKey;
            var attributeNamesCopy = attributeNames.slice();
			while((searchKey = attributeNamesCopy.shift())!==undefined && typeof elem[searchKey] !== 'undefined' ){
				elem = elem[searchKey];
				// console.log(elem);
			}
			// here can add values
			/** here we think that already find elem in list*/
			if(!attributeNamesCopy.length){
				newStorageElement.items.push(elem);
				if(isLike){
					if(!_this.getValueByKey(attributeNames)){
						return true;
					}else{
						return elem.toString().toLowerCase().indexOf(_this.getValueByKey(attributeNames).toLowerCase())!==-1;
					}
				}else if(initValue){
					return elem === initValue;
				}else 
					if(!_this.getValueByKey(attributeNames)){
						return true;
					}else{
						return elem == _this.getValueByKey(attributeNames);
					}
			}
			return false;
        });
        this.storage.push(newStorageElement);
        return this;
    }

    getFilteredItems(){
    	return this.filteredItems;
    }

    getInitItems(){
    	return this.initItems;
    }

    getValueByKey(key){
    	var element = this.getSearchElementByKey(key);
    	return (!element) ? false: element.value;
    }

    setValueByKey(key, value){
    	if(this.storage.length){
			this.storage = this.storage.map((data)=>{
    			if(JSON.stringify(data.key)==JSON.stringify(key)){
    				data.value = value;
    			}
    			return data;
    		});
		}
		return this;
    } 

    getItemsByKey(key){
		var element = this.getSearchElementByKey(key);
    	return (!element) ? []: element.items;
    }
    getSearchElementByKey(key){
		if(this.storage.length){
			var element = this.storage.find((data)=>{
    			return JSON.stringify(data.key)==JSON.stringify(key);
    		});
    		if(typeof element !== 'undefined')
    			return element;
		}
		return false;
    }
}