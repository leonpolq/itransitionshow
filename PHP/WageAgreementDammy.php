<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 8:48 AM
 */
class WageAgreementDammy
	extends
		WageAgreementCommonAbstract
	implements
		DefaultConfigurationInterface
{
	use DefaultValuesProviderTrait;
	public function getTitle() {
		return $this->_restaurantModel->WageAgreement->name;
	}
}