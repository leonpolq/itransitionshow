<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 3:58 PM
 */
class WageAgreementIDCC1747HourRateBankHolidays implements HourRateElementCalculationInterface
{
	/** @var  User */
	public $_user;
	/** @var  string */
	private $_date;
	public function __construct(User $user, $date) {
		$this->_user=$user;
		$this->_date=$date;
	}

	/**
	 * this functikon get holidays passed day
	 * and looking for a previouds day holiday
	 *
	 * after you can caltulate time between midnight and midnight and merge it together
	 *
	 * @return float
	 */
	public function calculateHourRate() {
		$date = (new DateTime($this->_date))->setTime(0,0,0);
		$dateStart = (new DateTime($this->_date))->setTime(0,0,0);

//		$holidayTomorrow = HolidaysList::getHolidayByInfo($this->_user->restaurant_id,
//			$date->add(new DateInterval('P1D'))->format(Tools::SQL_DATE_FORMAT));
		{
			$dateToday = (new DateTime($this->_date))->setTime(0,0,0);
			$dateStartToday = (new DateTime($this->_date))->setTime(0,0,0)->add(new DateInterval('P1D'));
			$holidayToday = HolidaysList::getHolidayByInfo($this->_user->restaurant_id, $dateToday->format(Tools::SQL_DATETIME_FORMAT));
			$arr[]=[
				'day'=>'this_day',
				'holiday'=>$holidayToday,
				'dateStart'=>$dateStartToday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
				'date'=>$dateToday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
			];
		}
		{
			$dateYesterday =(new DateTime($this->_date))->setTime(0,0,0)->sub(new DateInterval('P1D'));
			$dateStartYesterday = (new DateTime($this->_date))->setTime(0,0,0);
			$holidayYesterday = HolidaysList::getHolidayByInfo($this->_user->restaurant_id, $dateStartYesterday->format(Tools::SQL_DATETIME_FORMAT));
			$arr[]=[
				'day'=>'yesterday_day',
				'holiday'=>$holidayYesterday,
				'dateStart'=>$dateStartYesterday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
				'date'=>$dateYesterday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
			];
		}




		$arr= array_filter($arr);
		$arr= array_filter($arr, function (array $holidaysList){
			return !is_null($holidaysList['holiday']) && $holidaysList['holiday']->getAttribute('id')!=16;/** mai 1*/
		});

		$hours = array_filter(array_map(function (array $holidayConfig) use ($dateToday) {
			/** @var HolidaysList $holiday */
//			Tools::dump($holidayConfig);
//			Tools::dump(strtotime($this->_date));
//			Tools::dump(Tools::time_to_bd($dateToday->format(Tools::SQL_DATETIME_FORMAT), 'U'));

			$holiday = $holidayConfig['holiday'];
			if($holidayConfig['day']==='this_day'){
				$sql_mai1= "SELECT  sum(
                           UNIX_TIMESTAMP( IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.end)>0, :start_day2, actual_time.end))
                            -UNIX_TIMESTAMP(actual_time.start)
                            - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)>0, :start_day2, actual_time.break_end)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)>0, :start_day2, actual_time.break_end)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)>0, :start_day2, actual_time.break_start)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)>0, :start_day2, actual_time.break_start)))
                                - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)>0, :start_day2, actual_time.break_end2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)>0, :start_day2, actual_time.break_end2)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)>0, :start_day2, actual_time.break_start2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)>0, :start_day2, actual_time.break_start2)))
                                )/(60*60) as hours_actual
                        FROM `user` as `user`
                        JOIN `user_actual_time` as `actual_time` on actual_time.user_id = user.id AND actual_time.date=:dateActual
                        WHERE user.id=:user_id  GROUP BY actual_time.user_id";
				$connection=Yii::app()->db;
				$id = $this->_user->getAttribute('id');
				$command_header=$connection->createCommand($sql_mai1);
				$command_header->bindParam(":user_id", $id , PDO::PARAM_INT);
				$command_header->bindParam(":dateActual", $holidayConfig['date'], PDO::PARAM_STR);
				$command_header->bindParam(":start_day2",$holidayConfig['dateStart'],PDO::PARAM_STR);
				$hours=$command_header->queryScalar();
				return (float)$hours;
			}else if($holidayConfig['day']==='yesterday_day'){
				$sql_mai1= "SELECT  sum(
                           UNIX_TIMESTAMP( IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.end)<0, :start_day2, actual_time.end))
                            -UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.start)<0, :start_day2, actual_time.start))
                            - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)<0, :start_day2, actual_time.break_end)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)<0, :start_day2, actual_time.break_end)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)<0, :start_day2, actual_time.break_start)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)<0, :start_day2, actual_time.break_start)))
                                - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)<0, :start_day2, actual_time.break_end2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)<0, :start_day2, actual_time.break_end2)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)<0, :start_day2, actual_time.break_start2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)<0, :start_day2, actual_time.break_start2)))
                                )/(60*60) as hours_actual
                        FROM `user` as `user`
                        JOIN `user_actual_time` as `actual_time` on actual_time.user_id = user.id AND actual_time.date=:dateActual
                        WHERE user.id=:user_id  GROUP BY actual_time.user_id";
				$connection=Yii::app()->db;
				$id = $this->_user->getAttribute('id');
				$command_header=$connection->createCommand($sql_mai1);
				$command_header->bindParam(":user_id", $id , PDO::PARAM_INT);
				$command_header->bindParam(":dateActual", $holidayConfig['date'], PDO::PARAM_STR);
				$command_header->bindParam(":start_day2",$holidayConfig['dateStart'],PDO::PARAM_STR);
				$hours=$command_header->queryScalar();

				return (float)$hours;
			}
		}, $arr));
		return (float) array_sum($hours);
	}

    public function getTitle()
    {
        return Yii::t('model', 'Holidays');
    }

    public function getDescription()
    {
        return Yii::t('model', 'Holidays description');
    }

    public function getDate()
    {
        return $this->_date;
    }
}