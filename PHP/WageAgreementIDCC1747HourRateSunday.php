<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 3:58 PM
 */
class WageAgreementIDCC1747HourRateSunday implements HourRateElementCalculationInterface
{
	/** @var  User */
	public $_user;
	/** @var  string */
	private $_date;
	public function __construct(User $user, $date) {
		$this->_user=$user;
		$this->_date=$date;
	}
	public function calculateHourRate() {
		$date = new DateTime($this->_date);
		$dateSunday = new DateTime($this->_date);

		if(in_array($date->format('N'), [7/*, 1*/])){

		    if((int) $date->format('N') === 7 ){
                $dateSunday->add(new DateInterval('P1D'));
                $arr = [
                    [
                        'type'=>7,
                        'dateSunday'=>$dateSunday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
                        'date'=>$date->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
                    ]
                ];
            }
		    if((int) $date->format('N') === 1 ){
                $date->sub(new DateInterval('P1D'));
                $arr = [
                    [
                        'type'=>1,
                        'dateSunday'=>$dateSunday->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
                        'date'=>$date->format(Tools::SQL_DATE_FORMAT).' 00:00:00',
                    ]
                ];
            }


			$hours = array_filter(array_map(function ($dateConfig){
                $hours = .0;
			    if($dateConfig['type']===7){
                    $sql_mai1= "SELECT  sum(
                           UNIX_TIMESTAMP( IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.end)>0, :start_day2, actual_time.end))
                            -UNIX_TIMESTAMP(actual_time.start)
                            - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)>0, :start_day2, actual_time.break_end)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)>0, :start_day2, actual_time.break_end)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)>0, :start_day2, actual_time.break_start)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)>0, :start_day2, actual_time.break_start)))
                                - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)>0, :start_day2, actual_time.break_end2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)>0, :start_day2, actual_time.break_end2)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)>0, :start_day2, actual_time.break_start2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)>0, :start_day2, actual_time.break_start2)))
                                )/(60*60) as hours_actual
                        FROM `user` as `user`
                        JOIN `user_actual_time` as `actual_time` on actual_time.user_id = user.id AND actual_time.date=:dateActual
                        WHERE user.id=:user_id  GROUP BY actual_time.user_id";
                    $connection=Yii::app()->db;
                    $id = $this->_user->getAttribute('id');
                    $command_header=$connection->createCommand($sql_mai1);
                    $command_header->bindParam(":user_id", $id , PDO::PARAM_INT);
                    $command_header->bindParam(":dateActual", $dateConfig['date'], PDO::PARAM_STR);
                    $command_header->bindParam(":start_day2",$dateConfig['dateSunday'],PDO::PARAM_STR);
                    $hours=$command_header->queryScalar();/** +1 запрос*/
                }else if($dateConfig['type'] === 1){// implement for monday
                    $sql_mai1= "SELECT  sum(
                           UNIX_TIMESTAMP( IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.end)<0, :start_day2, actual_time.end))
                            -UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.start)<0, :start_day2, actual_time.start))
                            - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)<0, :start_day2, actual_time.break_end)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end)<0, :start_day2, actual_time.break_end)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)<0, :start_day2, actual_time.break_start)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start)<0, :start_day2, actual_time.break_start)))
                                - IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)<0, :start_day2, actual_time.break_end2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_end2)<0, :start_day2, actual_time.break_end2)))
                                + IF(UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)<0, :start_day2, actual_time.break_start2)) is null , 0, UNIX_TIMESTAMP(IF(TIMESTAMPDIFF(SECOND, :start_day2,actual_time.break_start2)<0, :start_day2, actual_time.break_start2)))
                                )/(60*60) as hours_actual
                        FROM `user` as `user`
                        JOIN `user_actual_time` as `actual_time` on actual_time.user_id = user.id AND actual_time.date=:dateActual
                        WHERE user.id=:user_id  GROUP BY actual_time.user_id";
                    $connection=Yii::app()->db;
                    $id = $this->_user->getAttribute('id');
                    $command_header=$connection->createCommand($sql_mai1);
                    $command_header->bindParam(":user_id", $id , PDO::PARAM_INT);
                    $command_header->bindParam(":dateActual", $dateConfig['date'], PDO::PARAM_STR);
                    $command_header->bindParam(":start_day2",$dateConfig['dateSunday'],PDO::PARAM_STR);
                    $hours=$command_header->queryScalar();/** +1 запрос*/
                }
				return (float)$hours;
			}, $arr));
			return (float) array_sum($hours);
		}else{
			return .0;
		}
	}
    public function getTitle()
    {
        return Yii::t('model', 'Snuday hour rate');
    }

    public function getDescription()
    {
        return Yii::t('model', 'Snuday hour rate description');
    }


    public function getDate()
    {
        return $this->_date;
    }
}