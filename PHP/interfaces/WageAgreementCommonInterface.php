<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 8:28 AM
 */
interface WageAgreementCommonInterface
{
	/**
	 * Inject dataProvider = RestaurantFactures
	 * WageAgreement constructor.
	 * @param Restaurant $restaurant
	 */
	public function __construct(Restaurant $restaurant);
	public function getFullTitle();
	public function getTitle();


	public function splitLateHours();
}