<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 11:58 AM
 */
interface DefaultValuesProviderInterface
{
	public function has($name);
	public function getValue($name, $default);
	public function setValue($name, $value);
}