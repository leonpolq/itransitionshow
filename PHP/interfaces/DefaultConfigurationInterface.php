<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 8:28 AM
 */
interface DefaultConfigurationInterface
{
	public function hasDefault($name);
	public function getValueDefault($name, $default);
	public function setValueDefault($name, $value);
}