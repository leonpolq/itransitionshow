<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 5/31/2017
 * Time: 8:28 AM
 */
abstract class WageAgreementCommonAbstract implements WageAgreementCommonInterface
{
	/** @var  Restaurant $_restaurantModel */
	protected $_restaurantModel;
	protected $_wageModel;
	/**
	 * Inject dataProvider = RestaurantFactures
	 * WageAgreement constructor.
	 * @param Restaurant $restaurant
	 */
	public function __construct(Restaurant $restaurant) {
		$this->_restaurantModel = $restaurant;
	}


	/**
	 * @return string
	 */
	public function getFullTitle() {
		return $this->_restaurantModel->WageAgreement->name.' '.$this->_restaurantModel->WageAgreement->description;
	}


	/**
	 * IMPLEMENT THIS
	 * @return string
	 */
	abstract public function getTitle();
}