<?php
namespace app\models\doctor;

use app\components\helpers\Tools;
use app\models\abstractClasses\InvoiceAccessInterface;
use app\models\DoctorCategory;
use app\models\DoctorInvoice;
use app\models\InvoiceProvider;
use app\models\TeleconseilChat;
use app\models\TeleconseilStatus;
use app\models\User;
use Yii;
use app\models\Category;
use app\models\abstractClasses\AbstractActiveRecord;
use yii\db\ActiveQuery;


/**
 * This is the model class for table "doctor".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $lastname
 * @property string $email
 * @property InvoiceProvider invoiceProvider
 * @property Category[] doctorCategory
 * @property User user
 * @property string comment
 */
class Doctor extends AbstractActiveRecord implements InvoiceAccessInterface {
    /**
     *
     */
    const KEY_COLUMN = 'id';
    /**
     *
     */
    const VALUE_COLUMN = 'fullName';
    const UNANSWERED_QUESTIONS_NUMBER = 1;

    /** for active record */
    public $ar_doctor_id;
    /**
     * @var
     */
    public $ar_id;
    /**
     * @var
     */
    public $ar_cost;
    /**
     * @var
     */
    public $ar_count;
    /**
     * @var
     */
    public $ar_tarif;
    /**
     * @var
     */
    public $ar_month;
    public $chat;
    public $count;
    public $ar_value;
    public $ar_value_tva;
    public $ar_ar_value_ht;
    /**
     * @var
     */
    public $ar_date;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id'], 'required'],
            [['name', 'lastname', 'email', 'comment', 'rpps', 'address',], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'lastname' => Yii::t('app', 'Lastname'),
            'email' => Yii::t('app', 'Email'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory(){
        return $this->hasMany(Category::className(), ['id' => 'doctor_id'])
            ->viaTable('doctor_category', ['category_id' => 'id']);
    }

	/**
	 * @param Category $category
	 * @return Doctor[]
	 */
	public static function activeHasCategory(Category $category) {
		$categories = Category::find()
			->joinWith(['doctors.user'])
			->andWhere(['user.status'=>User::STATUS_ACTIVE])
			->andWhere(['category.id'=>$category->id])
			->all();
		$doctorsOut = [];
		foreach ($categories as $c) {
			$doctors = $c->doctors;
			while(count($doctors)){
				array_push($doctorsOut, array_shift($doctors));
			}
		}
		return $doctorsOut;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorInvoice(){
        return $this->hasMany(DoctorInvoice::className(), ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorCategory(){
        return $this->hasMany(DoctorCategory::className(), ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChat(){
        return $this->hasMany(TeleconseilChat::className(), ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser(){
        return $this->hasOne(\app\models\User::className(), ['id'=>'user_id']);
    }

    /**
     * @return \app\models\abstractClasses\Collection
     */
    public function getWithoutInformation(){
        $users = self::find()
            ->all();

        $userIds = array_map(function($data){
            return $data->user_id;
        }, $users);

        $usersWithoutInformation = User::findCollection()
            ->alias('u')
            ->innerJoin(['a'=>'auth_assignment'], 'a.user_id = u.id and a.item_name="doctor"')
            ->where(['not in', 'u.id', $userIds])
            ->collection();

        return $usersWithoutInformation;
    }
    /**
     * @inheritdoc
     * @return DoctorQuery the active query used by this AR class.
     */
    public static function find(){
        return new DoctorQuery(get_called_class());
    }

    /**
     * @return string
     */
    public function getFullName(){
        return $this->name.' '.$this->lastname;
    }

    /**
     * @param $category_id
     */
    public function createCategoryById($category_id)
    {
        $doctorCategory = new DoctorCategory();
        $doctorCategory->category_id = $category_id;
        $this->link('doctorCategory', $doctorCategory);
    }

    /**
     * @param $category_id
     */
    public function deleteCategoryById($category_id)
    {
        $doctorCategory = new DoctorCategory();
        $doctorCategory->category_id = $category_id;
        $this->link('doctorCategory', $doctorCategory);
    }

    /**
     * @param array $categoriesId
     */
    public function attacheCategories(array $categoriesId){
        $categoryWithKeys = [];
        foreach ($this->doctorCategory as $item) 
            $categoryWithKeys[$item->category_id]=$item;
        
        $categoriesExistedIds = array_keys($categoryWithKeys);

        $delete = array_diff($categoriesExistedIds, $categoriesId);
        $create = array_diff($categoriesId, $categoriesExistedIds);
        
        array_map(function($i){
            $this->createCategoryById($i);
        }, $create);
        /** @var  Category[] $categoryWithKeys */
        array_map(function($i) use ($categoryWithKeys) {
            $categoryWithKeys[$i]->delete();
        }, $delete);

    }

    /**
     * @return mixed
     */
    public function getEmail(){
        return $this->getUser()->one()->email;
    }

    /**
     * @return array
     */
    public function  getPublicAttributes()
    {
        return array_intersect_key($this->getAttributes(), array_flip(['name', 'lastname', 'comment', 'rpps', 'address',]));
    }

    /**
     * @return array
     */
    public function getInvoicesList(){
        return array_values($this->invoiceProvider->getDoctorInvoicesList());
    }

    /**
     * @param $invoiceId
     * @return mixed
     */
    public function getInvoicePdf($invoiceId){
        return $this->invoiceProvider->getDoctorInvoicePdf($invoiceId);
    }
    /**
     * @param InvoiceProvider $invoiceProvider
     * @return $this
     */
    public function setInvoiceProvider(InvoiceProvider $invoiceProvider){
        $invoiceProvider->doctorId = $this->id;
        $this->invoiceProvider = $invoiceProvider;
        return $this;
    }

    /**
     * @param bool $includeAll
     * @return array
     */
    public function getCategoriesArray($includeAll = false){
        $doctorCategoryModels = $this->doctorCategory;
        $doctorCategories = \yii\helpers\ArrayHelper::getColumn($doctorCategoryModels, 'category_id');
        if($includeAll)
            $categories = array_map(function ($data) use ($doctorCategories){
                /** @var Category $data */
                $dataOut = $data->getAttributes();
                $dataOut['active'] = array_search($data->id, $doctorCategories)!==false;
                return $dataOut;
            }, Category::find()->all());
        else
            $categories = array_map(function (DoctorCategory $data){
                $category = $data->category;
                return ($category!==null)? $category->getAttributes() : [];
            }, $doctorCategoryModels);


        return $categories;

    }

    /**
     * @return bool
     */
    public function exceedUnclosedQuestionsLimit()
    {
        $chats = $this->getUnclosedQuestions();
        if(count($chats)>=Doctor::UNANSWERED_QUESTIONS_NUMBER)
            return true;
        else
            return false;
            
    }

    /**
     * @return \app\models\TeleconseilChat[]|array
     */
    public function getUnclosedQuestions()
    {
        $chats = TeleconseilChat::find()
            ->andWhere(['teleconseil_chat.doctor_id' => $this->id])
            ->getLastStatusEqual(TeleconseilStatus::STATUS_DOCTOR_GET_QUESTION)
            ->all();
        return $chats;
    }
    public function getActiveQuestions()
    {
        $chats = TeleconseilChat::find()
            ->andWhere(['teleconseil_chat.doctor_id' => $this->id])
            ->getLastStatusEqual(TeleconseilStatus::STATUS_DOCTOR_GET_QUESTION)
            ->all();
        return $chats;
    }

    /**
     * @return \app\models\TeleconseilChat[]|array
     */
    public function getPatientResponses () {
        $chats = TeleconseilChat::find()
            ->andWhere(['teleconseil_chat.doctor_id' => $this->id])
            ->getLastStatusEqual(
                TeleconseilStatus::STATUS_PATIENT_ANSWERED,
                [TeleconseilStatus::STATUS_PAYED]
            )
            ->all();
        return $chats;
    }

	/**
	 * @param Category $category
	 * @return bool
	 */
	public function hasCategory(Category $category) {
		return (bool) $this->getCategory()->andWhere(['category.id'=>$category->id])->count();
    }

	/**
	 * @return TeleconseilChat[]|array
	 */
    public function getStatistic(){
		return TeleconseilChat::getStatisticDataByDoctorId($this->id);
	}
}

























