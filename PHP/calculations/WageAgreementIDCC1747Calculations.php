<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 10:06 AM
 */
class WageAgreementIDCC1747Calculations
{
	/**
	 * return total amount
	 * @param RestaurantFactures $facturePropvider
	 * @param $id
	 * @param $user_id
	 * @param $start
	 * @param $end
	 * @param $user
	 * @param $period
	 * @return float|int
	 */
	public function facture__nightHours(RestaurantFactures $facturePropvider, $id,$user_id,$start,$end,$user,$period){
		$c=new CDbCriteria();
		$c->with=['user'];
		$c->compare('user.restaurant_id', $id);
		$c->compare('user.id', $user_id);
		$c->addBetweenCondition('t.date',$start,$end);
		$info_actual=UserActualTime::model()->findAll($c);
//		$debug = Restaurant::get_late_days_summ($info_actual) ;
		$result = Tools::check_array(Restaurant::get_late_days_summ($info_actual),[$user_id, 'sum' , 'total']);
		return $result/(60*60);
	}

	/**
	 * @param RestaurantFactures $facturePropvider
	 * @param $id
	 * @param $user_id
	 * @param $start
	 * @param $end
	 * @param $user
	 * @param $period
	 * @return float|int
	 */
	public function facture__holidayNumber(RestaurantFactures $facturePropvider, $id,$user_id,$start,$end,$user,$period){
//		$const = $facturePropvider->getConstantParameteres('holiday_number');
//		if(empty($const))
//			return 0;
		$result = $facturePropvider->callCommonFactureFunctionWithParams(
			$user,
			'holiday_number',
			$period,
			$id,
			$user_id,
			$start,
			$end
		);
		//getWage()->getCalculationProvider()
		return $result;
	}

	public function facture__hs_125(RestaurantFactures $facturePropvider, $id,$user_id,$start,$end,$user,$period){
		$full_time_contract=(isset($user->full_time) && $user->full_time==1)? true : false;
		if(!$full_time_contract)/** hc only for part time*/
			return 0;
		if((int)$user->restaurant->hs_calculation_type === 2){
			list($hc10, $hcSums, $HS, $HSSums, $weeklyWorkingHours, $weeklyWorkingHoursSum) =
				(new RestaurantFactures($id, true, ['start' => Tools::time_to_bd($start, 'U'), 'end' => Tools::time_to_bd($end, 'U')], true))->getHCAndHSDates($user,$user_id, $period, Tools::time_to_bd($start, 'U'), Tools::time_to_bd($end, 'U'));
			return $HSSums['hs125'];
		}else{
			return $facturePropvider->getQteCommonFull($id,$user_id,$start,$end,$user,$period, 'hs125');
		}
	}
	public function facture__hs_150(RestaurantFactures $facturePropvider, $id,$user_id,$start,$end,$user,$period){
		$full_time_contract=(isset($user->full_time) && $user->full_time==1)? true : false;
		if(!$full_time_contract)/** hc only for part time*/ 
			return 0;
		if((int)$user->restaurant->hs_calculation_type === 2){
			list($hc10, $hcSums, $HS, $HSSums, $weeklyWorkingHours, $weeklyWorkingHoursSum) =
				(new RestaurantFactures($id, true, ['start' => Tools::time_to_bd($start, 'U'), 'end' => Tools::time_to_bd($end, 'U')], true))->getHCAndHSDates($user,$user_id, $period, Tools::time_to_bd($start, 'U'), Tools::time_to_bd($end, 'U'));
			return $HSSums['hs150'];
		}else{
			return $facturePropvider->getQteCommonFull($id,$user_id,$start,$end,$user,$period, 'hs150');
		}
	}
	public function facture__sunday_hours(RestaurantFactures $facturePropvider, $id,$user_id,$start,$end,$user,$period){
		/** @var User $user */
		//todo: check that correct interval
		$days = $facturePropvider->{'monthDays'.$period};
		$lastDay = (new DateTime())->setTimestamp(end($days));
		// add one more day - sunday
		// if days finished in saturday
		if((int) $lastDay->format("N") === 6){
			$days[] = ((int)$lastDay->format('U')) + Tools::ONE_DAY_SECONDS;
		}
		//call calculation throught user
		$resultOut = [];
		// walk throught days and create result/debug array
		foreach ($days as $day) {
			$user->eraseCalculatedHourRates();
			$result = $user->getCaltulatedHourRate(Tools::time_to_bd($day), 'sunday');
			$resultOut[$day] = (!empty($result)) ? $result['hours'] : 0;
		}
		//return sum value
		return array_sum($resultOut);
	}
}