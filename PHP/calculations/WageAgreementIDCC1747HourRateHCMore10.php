<?php

/**
 * Created by PhpStorm.
 * User: Pavel Leonov
 * Date: 6/1/2017
 * Time: 3:58 PM
 */
class WageAgreementIDCC1747HourRateHCMore10 implements HourRateElementCalculationInterface
{
	/** @var  User */
	public $_user;
	/** @var  string */
	private $_date;
	public function __construct(User $user, $date) {
		$this->_user=$user;
		$this->_date=$date;
	}
	public function calculateHourRate() {
		$date = new DateTime($this->_date);
		$dateEnd = new DateTime($this->_date);
		$dateEnd->add(new DateInterval('P1D'));
        $result =
            $this
            ->_user
            ->restaurant
            ->getRestaurantFacture()
            ->callFactureVariableForPeriod('hc_more_10', $date, $dateEnd, $this->_user);
		return (float) $result;
	}
    public function getTitle()
    {
        return Yii::t('model', 'HC more 10');
    }

    public function getDescription()
    {
        return Yii::t('model', 'HC more 10 description');
    }


    public function getDate()
    {
        return $this->_date;
    }
}