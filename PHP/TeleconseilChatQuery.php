<?php

namespace app\models;
use app\components\helpers\TimeHelper;
use app\components\helpers\Tools;
use app\models\abstractClasses\AbstractActiveQuery;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * This is the ActiveQuery class for [[TeleconseilChat]].
 *
 * @see TeleconseilChat
 */
class TeleconseilChatQuery extends AbstractActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TeleconseilChat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeleconseilChat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $status
     * @param bool $last
     * @return $this
     */
    public function withStatus($status, $last = true){
        if($last)
            return $this->where(['in', 'status_id', $status]);
//        else
//            return $this->
    }

    /**
     * @return $this
     */
    public function active(){
        return $this->andWhere(['not in', 'teleconseil_chat.status_id', [ TeleconseilStatus::STATUS_PATIENT_DELETE_CHAT ]]);
    }
    /**
     * @return $this
     */
    public function vacant()
    {
        return $this
            ->alias('teleconseil_chat')
            ->withStatus([TeleconseilStatus::STATUS_OPEN, TeleconseilStatus::STATUS_PAYED])
            ->joinWith(['category', 'initMessage', 'patient', 'status', ])
            ->orderBy(['teleconseil_chat.id' => SORT_DESC ])
            ->andWhere('teleconseil_chat.doctor_id is null');
    }

    /**
     * @return $this
     */
    public function latest(){
        return $this->orderBy(['created'=>SORT_DESC])->limit(1);
    }

    /**
     * @return $this
     */
    public function newest(){
        return $this->orderBy(['created'=>SORT_ASC])->limit(1);
    }

    /**
     * @return $this
     */
    public function isPayed()
    {
        return $this->joinWith('token', true, 'INNER JOIN');
    }

    /**
     * @return $this
     */
    public function isFree()
    {
        return $this
            ->andWhere(['not in', 'teleconseil_chat.id', PaymentToken::find()->select('chat_id')])
            //->andWhere(['teleconseil_chat.tarif_id'=>Tarif::FREE_TARIF])//removed
            ;
    }

    /**
     * @param array $statuses
     * @return $this
     */
    public function checkIsStatusIn(array $statuses = []){
        return $this->leftJoin(['checkStatuses'=>
                (new Query())
                    ->from([
                        TeleconseilChatHistory::find()
                            ->alias('checkStatusesHistory')
                            ->andWhere(['in', 'checkStatusesHistory.status_id', $statuses])
                            ->groupBy(['checkStatusesHistory.chat_id'])
                    ])
            ], "checkStatuses.chat_id = ".TeleconseilChat::tableName().".id")
            ;//->groupBy([TeleconseilChat::tableName().".id"]);
    }
    /**
     * @return $this
     */
    public function canUserViewDoctorAnswer()
    {
        return $this
            ->joinOpenStatuses()
            ->select([
                TeleconseilChat::tableName().'.*',
                'canUserViewAnswer' =>'if(
                token24.id IS NOT NULL 
                AND 
                token24.cost is not null,
                1, 
                    IF (
                        token48.id IS NOT NULL 
                        &&
                        token48.cost is not null
                        && historyStatuses.id IS NOT NULL 
                        && historyStatuses.datetime < DATE_SUB(now(), INTERVAL 48 hour), 1, 0))',
                'initTime'=>'UNIX_TIMESTAMP(historyStatuses.datetime)' ,
                'unpayed'=>'if(
                    COALESCE(token24.cost, token48.cost) IS NULL, 
                    1, 
                    0
                )',
            ])
            ->leftJoin(PaymentToken::tableName().' as token24', '
                token24.chat_id = '.TeleconseilChat::tableName().'.id
                AND 
                token24.tarif_id = :tarif24
            ')
            ->leftJoin(PaymentToken::tableName().' as token48', '
                token48.chat_id = '.TeleconseilChat::tableName().'.id
                AND 
                token48.tarif_id = :tarif48
            ')
            ->params(array_replace($this->params, [
                ':tarif24'=>Tarif::MAX_TARIF,
                ':tarif48'=>Tarif::TARIF_48h,
            ]))
            ->joinWith(['token'])
            /**
             * that means we select by left join
             * existense closed status
             * If status closed we can already show doctors answer
             */
            ->checkIsStatusIn([TeleconseilStatus::STATUS_CLOSE]);
    }



    /**
     * @return $this
     */
    public function detectTarifAnswerStatus()
    {
        return $this->addSelect([
                'statusTarif' =>  'if(payment_token.id is not null, '.TeleconseilChat::tableName().'.tarif_id, 0)'
            ])
            ->joinWith(['token']);
    }


    /**
     * @param $timeFormated
     * @return $this
     */
    public function isNotClosedSince($timeFormated)
    {
        return $this->joinWith(['teleconseilChatHistory as history'=>function(ActiveQuery $query) use ($timeFormated){
            $query->onCondition([
                'IN',
                'history.chat_id',
                TeleconseilChatHistory::find()
                    ->alias('history_inner')
                    ->select('chat_id')
                    ->andWhere(
                        'history_inner.datetime < DATE_SUB(:time,INTERVAL :hour hour) AND (history_inner.status_id = :status OR history_inner.status_id = :status2)',
                        [
                            ':time'=>$timeFormated,
                            ':status'=>TeleconseilStatus::STATUS_OPEN,//for free
                            ':status2'=>TeleconseilStatus::STATUS_WAIT_PAYMENT,//for payed
                            ':hour'=>TeleconseilChat::EXPIRATION_HOURS_NUMBER,
                        ]
                    )->andWhere(['in', 'history_inner.chat_id',
                        TeleconseilChatHistory::find()
                            ->alias('inner')
                            ->select('chat_id')
                            ->andWhere(
                                [
                                    'IN', 'inner.status_id' , [TeleconseilStatus::STATUS_OPEN, TeleconseilStatus::STATUS_WAIT_PAYMENT]//TeleconseilStatus::STATUS_DOCTOR_ANSWERED
                                ]
                            )->andWhere(['NOT IN', 'inner.chat_id',
                                TeleconseilChatHistory::find()
                                    ->alias('inner2')
                                    ->select('chat_id')
                                    ->andWhere(['in', 'inner2.status_id',
                                        [TeleconseilStatus::STATUS_CLOSE]
                                    ])
                            ])
                    ])
            ]);
        }], true, 'INNER JOIN')
        ->groupBy('history.chat_id') ;
    }

    /**
     *
     * @return $this
     */
    public function joinOpenStatuses()
    {
        return $this->joinWith(['teleconseilChatHistory as historyStatuses' => function(ActiveQuery $query){
            $query->onCondition([
                'IN',
                'historyStatuses.id',
                TeleconseilChatHistory::find()
                    ->alias('history_inner2')
                    ->select('id')
                    ->andWhere(
                        '  
                            (
                                history_inner2.status_id = :status 
                                    OR 
                                history_inner2.status_id = :status2
                            )
                            ',
                        [
                            ':status'=>TeleconseilStatus::STATUS_OPEN,//for free
                            ':status2'=>TeleconseilStatus::STATUS_WAIT_PAYMENT,//for payed
                            //':48h_tarif'=>Tarif::TARIF_48h,
                            //':statuses'=> implode(', ', $statuses),
                        ]
                    )
            ]);
        }], true, 'LEFT JOIN');
    }

    /**
     *
     * @return $this
     */
    public function joinPaymentStatuses()
    {
        return $this->joinWith(['teleconseilChatHistory as historyStatuses' => function(ActiveQuery $query){
            $query->onCondition([
                'IN',
                'historyStatuses.chat_id',
                TeleconseilChatHistory::find()
                    ->alias('history_inner2')
                    ->select('chat_id')
                    ->andWhere(
                        'history_inner2.datetime > DATE_SUB(now(), INTERVAL 48 hour) 
                            AND  
                            '.TeleconseilChat::tableName().'.tarif_id = :48h_tarif
                            AND 
                            (
                                history_inner2.status_id = :status OR history_inner2.status_id = :status2
                            )',
                        [
                            ':status'=>TeleconseilStatus::STATUS_OPEN,//for free
                            ':status2'=>TeleconseilStatus::STATUS_WAIT_PAYMENT,//for payed
                            ':48h_tarif'=>Tarif::TARIF_48h,
                            //':statuses'=> implode(', ', $statuses),
                        ]
                    )
            ]);
        }], true, 'INNER JOIN');
    }
    /**
     * @param $timeFormated
     * @return $this
     */
    public function isNotPayedSince($timeFormated){
        /**
        SELECT
        `teleconseil_chat`.* , count(history.id) as historyCount
        FROM
        `teleconseil_chat`
        INNER JOIN `teleconseil_chat_history` `history` ON (
            `teleconseil_chat`.`id` = `history`.`chat_id`
        )
        group by history.chat_id
        HAVING count(historyCount) > 6
        */
        return $this->joinWith(['teleconseilChatHistory as history'=>function(ActiveQuery $query) use ($timeFormated){
            $query->onCondition([
                'IN',
                'history.chat_id',
                TeleconseilChatHistory::find()
                    ->alias('history_inner')
                    ->select('chat_id')
                    ->andWhere(
                        'history_inner.datetime < DATE_SUB(:time,INTERVAL :hour hour) AND (history_inner.status_id = :status2)',
                        [
                            ':time'=>$timeFormated,
                            ':status2'=>TeleconseilStatus::STATUS_WAIT_PAYMENT,//for payed
                            ':hour'=>TeleconseilChat::EXPIRATION_PAYMENT_HOURS_NUMBER,
                        ]
                    )
            ]);
        }], true, 'INNER JOIN')
        ->andWhere(TeleconseilChat::tableName().'.doctor_id is NULL')
        ->groupBy('history.chat_id')
        ->having('count(history.id)=1');//only one status
    }

    /**
     * @param $timeFormated
     * @return $this
     */
    public function isNotAnsweredSince($timeFormated)
    {
        return $this
            ->getLastStatusEqual(
                TeleconseilStatus::STATUS_DOCTOR_GET_QUESTION,
                [TeleconseilStatus::STATUS_PAYED],
                [
                    [
                        'andWhere', [
                            'outer.datetime < DATE_SUB(:time,INTERVAL :minute minute)',
                            [
                                ':time' => $timeFormated,
                                ':minute'=>TeleconseilChat::EXPIRATION_ANSWER_MINUTES,
                            ]
                        ]
                    ]
                ]
            )
            ->groupBy('history.chat_id');
    }

    /**
     * @return $this
     */
    public function currentStatusIsNotClosed(){
        return $this->andWhere(TeleconseilChat::tableName().'.status_id!=:statusClosed', 
            ['statusClosed'=>TeleconseilStatus::STATUS_CLOSE]);
    }
    
    /**
     * @param $status
     * @param array $exclude
     * @param array $addWhereToInner
     * @return $this
     */
    public function getLastStatusEqual($status, $exclude = [TeleconseilStatus::STATUS_PAYED], $addWhereToInner = []){
        $queryInner = (new \yii\db\Query)
            ->from([
                'outer' =>
                    (new \yii\db\Query)
                        ->from([
                            'inner' =>
                                TeleconseilChatHistory::find()
                                    ->addSelect(['*','maxDate'=>'MAX(teleconseil_chat_history.datetime)'])
                                    ->andWhere(['not in', 'status_id', $exclude])
                                    ->groupBy(['chat_id'])
                                    ->orderBy(['chat_id' => SORT_DESC, 'datetime' => SORT_DESC])
                        ])
                        ->groupBy(['inner.chat_id'])
            ])
            ->select('chat_id')
            ->andWhere(['outer.status_id' => $status]);
        
        if(!empty($addWhereToInner)){
            array_walk($addWhereToInner, function($data) use ($queryInner){
                call_user_func_array([$queryInner, $data[0]], $data[1]);
            });
        }

        return $this->joinWith(['teleconseilChatHistory as history'=>function(ActiveQuery $query) use ($status, $exclude, $queryInner){
            $query->onCondition([
                'IN',
                'history.chat_id',
                $queryInner
            ]);
        }], true, 'INNER JOIN');
    }
    /**
     * @param $status
     * @param array $exclude
     * @param array $addWhereToInner
     * @return $this
     */
    public function joinInitMessage(){

        return $this->joinWith(['teleconseilMessages as initMessage'=>function(ActiveQuery $query){
            $query->onCondition([
                'initMessage.reply_to'=>TeleconseilMessage::START_MESSAGE,
            ]);
        }], true, 'INNER JOIN');
    }

    /**
     * @return $this
     */
    public function shouldDoctorSeeAnswer(){
        $this->getIsLastStatusEqual(
            TeleconseilStatus::STATUS_PATIENT_ANSWERED,
            [TeleconseilStatus::STATUS_PAYED],
            [],
            "PatientAnswered"
        )
        

        ;
        return $this;
    }

    /**
     * @param $status
     * @param array|null $exclude daful
     * @param array $addWhereToInner
     * @param string $statisSufix
     * @return $this
     */
    public function getIsLastStatusEqual($status, $exclude = [TeleconseilStatus::STATUS_PAYED], $addWhereToInner = [], $statusSufix = ""){
        $queryInner = (new \yii\db\Query)
            ->from([
                'isHasStatusOuter' =>
                    (new \yii\db\Query)
                        ->from([
                            'isHasStatusInner' =>
                                TeleconseilChatHistory::find()
                                    ->addSelect(['*','maxDate'=>'MAX(teleconseil_chat_history.datetime)'])
                                    ->andWhere(['not in', 'status_id', $exclude])
                                    ->groupBy(['chat_id'])
                                    ->orderBy(['chat_id' => SORT_DESC, 'datetime' => SORT_DESC])
                        ])
                        ->groupBy(['isHasStatusInner.chat_id'])
            ])
            ->select('chat_id')
            ->andWhere(['isHasStatusOuter.status_id' => $status]);

        if(!empty($addWhereToInner)){
            array_walk($addWhereToInner, function($data) use ($queryInner){
                call_user_func_array([$queryInner, $data[0]], $data[1]);
            });
        }

        return $this
            ->addSelect(['isChatHasRequestedStatus'.$statusSufix=>'
                if(isHasStatusHistory.id is not null, 1, 0)
            '])
            ->joinWith(['teleconseilChatHistory as isHasStatusHistory'=>function(ActiveQuery $query) use ($status, $exclude, $queryInner){
            $query->onCondition([
                'IN',
                'isHasStatusHistory.chat_id',
                $queryInner
            ]);
        }], true, 'LEFT JOIN');
    }

	/**
	 * @return $this
	 */
    public function getPaymentTokensForChat(){
        return $this
            ->andWhere(['chat_id'=>$this->id]) ;
    }
}
